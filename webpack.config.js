const path = require('path');

module.exports = {
    resolve: {
        extensions: ['.js', '.vue', '.scss'],
        alias: {
            '@': path.join(__dirname, '/resources'),
        },
    },
    output: {
        // template path
        publicPath: 'assets/dist/',
        chunkFilename: 'js/polyfills/[name].js',
    },
};
