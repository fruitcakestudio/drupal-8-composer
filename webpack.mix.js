let mix = require('laravel-mix');
const config = require('./webpack.config');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.webpackConfig(config);

// Drupal
mix.setPublicPath('web/themes/[name]')
    .setResourceRoot('/themes/[name]/')
    .sass('source/scss/main.scss', 'css');